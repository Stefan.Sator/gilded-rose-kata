# gilded-rose-kata

sequence of hotkeys

alt+1 project editor
alt+shift+f10 run config
f2 go to problem
ctrl+f5 run config again
alt+k edit commit
alt+i commit
alt+shift+f11 run config with coverage, this is a custom hotkey
navigate to pom and go to second line
alt+insert add dependency
search for approvaltests
ctrl+shift+o load maven changes
ctrl+alt+a add file to git

other useful hotkeys
ctrl+w extend selection
ctrl+alt+v extract variable
ctrl+alt+m extract methode

all hotkeys related to run
when there is no configuration:
alt+shift+f10 run
alt+shift+f11 run with coverage

when there is a configuration
shift+f9 run in debug mode
shift+f10 run

Original video
https://www.youtube.com/watch?v=wp6oSVDdbXQ&t=542s

Best tests:
https://www.youtube.com/watch?v=vMww6pV6P7s

Series:
https://www.youtube.com/watch?v=zyM2Ep28ED8

